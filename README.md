# Switch manifest for Halium 11

### Patching
Basic:
* Repopick topics `icosa-bt-r` off Lineage Gerrit
* Repopick commits `308972` off Lineage Gerrit
* Apply all patches to their respective directories (from patches folder)

### Products
* `nx_tab`: Standard tablet Android with Nvidia enhancements, based on LineageOS

### Notes
* In progress--may not boot
